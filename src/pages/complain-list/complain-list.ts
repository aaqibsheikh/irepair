import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import 'rxjs/add/operator/map';

import { AuthService } from '../../providers/auth-service/auth-service';
import { ComplainPage } from '../complain/complain';
import { ComplainDiscussionPage } from '../complain-discussion/complain-discussion';
import * as firebase from 'Firebase';

 @IonicPage()
 @Component({
	selector: 'page-complain-list',
	templateUrl: 'complain-list.html',
 })
 export class ComplainListPage {

 	responseData : any;
 	complain_list:any;
 	no_complain:string;
 	ref = firebase.database().ref('irepair/');

	constructor(public authService:AuthService, public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public loading: LoadingController){
		this.getComplain();
	}

	addComplain(){
		this.navCtrl.push(ComplainPage);
	}

	getComplain(){

		let loader = this.loading.create({
			content: 'Processing please wait…'
		});

		loader.present().then(() => {

			this.authService.getComplain().then((result) => {
				this.responseData = result;
				
				if(this.responseData.status == 'success'){

					if(this.responseData.data == ''){
						this.no_complain = 'No Complain Created Yet';
						loader.dismiss();
					}else{
						this.complain_list = this.responseData.data;
						loader.dismiss();
					}

				}
				else{
					let alert = this.alertCtrl.create({

						title: 'ERROR',

						subTitle: this.responseData.error,

						buttons: ['OK']

					});

					alert.present();
					loader.dismiss()
				}

			}, (err) => {
				// Error log
				loader.dismiss()
				let alert = this.alertCtrl.create({

					title: 'ERROR',

					subTitle: err.message,

					buttons: ['OK']

				});

				alert.present();
			});

		});
	}

	openComplain(complain){
		let newData = this.ref.push();
		newData.set({
			complain_name:complain
		});
		this.navCtrl.push(ComplainDiscussionPage, {complain:complain});
	}

}
