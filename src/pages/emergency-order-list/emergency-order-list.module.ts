import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EmergencyOrderListPage } from './emergency-order-list';

@NgModule({
  declarations: [
    EmergencyOrderListPage,
  ],
  imports: [
    IonicPageModule.forChild(EmergencyOrderListPage),
  ],
})
export class EmergencyOrderListPageModule {}
