import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service/auth-service';
import { EmergencyOrderPage } from '../emergency-order/emergency-order';


@IonicPage()
@Component({
  selector: 'page-emergency-order-list',
  templateUrl: 'emergency-order-list.html',
})
export class EmergencyOrderListPage {

  responseData : any;
 	emergency_order_list:any;
 	no_emergency_order:string;

	constructor(public authService:AuthService, public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public loading: LoadingController){
		this.getEmergencyOrderList();
	}

	addEmergencyOrder(){
		this.navCtrl.push(EmergencyOrderPage);
	}

	getEmergencyOrderList(){

		let loader = this.loading.create({
			content: 'Processing please wait…'
		});

		loader.present().then(() => {

			this.authService.getEmergencyOrderList().then((result) => {
				this.responseData = result;
				if(this.responseData.status == 'success'){

					if(this.responseData.data == ''){
						this.no_emergency_order = 'No Emergency Order Created Yet';
						loader.dismiss();
					}else{
						this.emergency_order_list = this.responseData.data;
						loader.dismiss();
					}

				}
				else{
					let alert = this.alertCtrl.create({

						title: 'ERROR',

						subTitle: this.responseData.error,

						buttons: ['OK']

					});

					alert.present();
					loader.dismiss()
				}

			}, (err) => {
				// Error log
				loader.dismiss()
				let alert = this.alertCtrl.create({

					title: 'ERROR',

					subTitle: err.message,

					buttons: ['OK']

				});

				alert.present();
			});

		});
	}
}