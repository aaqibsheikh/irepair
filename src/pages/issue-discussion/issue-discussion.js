var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Content } from 'ionic-angular';
import * as firebase from 'Firebase';
var IssueDiscussionPage = /** @class */ (function () {
    // data = { type:'', nickname:'', message:'',roomname:'' };
    // chats = [];
    // roomkey:string;
    // nickname:string;
    // offStatus:boolean = false;
    function IssueDiscussionPage(navCtrl, navParams) {
        // this.issue_title = this.navParams.get('data');
        // this.addRoom(this.issue_title);
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.data = { roomname: '' };
        this.ref = firebase.database().ref('irepair/');
        // let newData = this.ref.push();
        // newData.set({
        // 	issue_title:this.issue_title
        // });
        // console.log(this.issue_title);
        // console.log(newData);
        // this.roomkey = this.navParams.get("key") as string;
        // this.nickname = this.navParams.get("nickname") as string;
        // this.data.type = 'message';
        // this.data.nickname = this.nickname;
    }
    IssueDiscussionPage.prototype.ionViewDidLoad = function () {
    };
    IssueDiscussionPage.prototype.addRoom = function (issue_title) {
        console.log(this.issue_title);
        var newData = this.ref.push();
        newData.set({
            roomname: issue_title
        });
    };
    IssueDiscussionPage.prototype.ngOnInit = function () {
        console.log();
    };
    __decorate([
        ViewChild(Content),
        __metadata("design:type", Content)
    ], IssueDiscussionPage.prototype, "content", void 0);
    IssueDiscussionPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-issue-discussion',
            templateUrl: 'issue-discussion.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams])
    ], IssueDiscussionPage);
    return IssueDiscussionPage;
}());
export { IssueDiscussionPage };
//# sourceMappingURL=issue-discussion.js.map