import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IssueDiscussionPage } from './issue-discussion';

@NgModule({
  declarations: [
    IssueDiscussionPage,
  ],
  imports: [
    IonicPageModule.forChild(IssueDiscussionPage),
  ],
})
export class IssueDiscussionPageModule {}
