import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EmergencyOrderPage } from './emergency-order';

@NgModule({
  declarations: [
    EmergencyOrderPage,
  ],
  imports: [
    IonicPageModule.forChild(EmergencyOrderPage),
  ],
})
export class EmergencyOrderPageModule {}
