import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController } from 'ionic-angular';
import {Http, Headers, RequestOptions}  from '@angular/http';
import { LoadingController } from 'ionic-angular';
import { RotaPage } from '../rota/rota' 
import 'rxjs/add/operator/map';
import { AuthService } from '../../providers/auth-service/auth-service';

@IonicPage()
@Component({
	selector: 'page-emergency-order',
	templateUrl: 'emergency-order.html',
})
export class EmergencyOrderPage {

	product:any;
	productNames: any;
	productOptions:any;
	productDescription:string;
	responseData : any;
	product_detail:any = {detail:'',user_id_fk:'',shop_id_fk:''};
	shop_id:any;
	constructor(public authService:AuthService, public loading: LoadingController, public alertCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams) {
		
	}


	placeEmergencyOrder(){
		//Getting User ID from local storage
		this.shop_id = JSON.parse(localStorage.getItem('shop_id'));
		let user_info = JSON.parse(localStorage.getItem('userData'));
		this.product_detail.user_id_fk = user_info.user.id;

		if(this.shop_id == null)
			this.product_detail.shop_id_fk = 0;

		let loader = this.loading.create({

			content: 'Processing please wait…',

		});

		loader.present().then(() => {
			this.authService.placeEmergencyOrder(this.product_detail).then((result) => {
				this.responseData = result;
				
				if(this.responseData.status == 'success'){

					if(this.responseData.data == ''){
						loader.dismiss();
						let alert = this.alertCtrl.create({

							title: 'Order Place Successfully',
							buttons: [{
								text:'OK',
								handler: () => {
						          this.navCtrl.setRoot(RotaPage);
						        }
							}]
						});

						alert.present();
					}else if(this.responseData.data == 'You cannot place order at this moment'){
						loader.dismiss();
						let alert = this.alertCtrl.create({

							title: 'Message',
							subTitle: this.responseData.data,
							buttons: [{
								text:'OK',
								handler: () => {
						          this.navCtrl.setRoot(RotaPage);
						        }
							}]
						});
						alert.present();
					}
					else{

					}



				}
				else{
					let alert = this.alertCtrl.create({

						title: 'ERROR',

						subTitle: this.responseData.error,

						buttons: ['OK']

					});

					alert.present();
					loader.dismiss()
				}

			}, (err) => {
				// Error log
				loader.dismiss()
				let alert = this.alertCtrl.create({

					title: 'ERROR',

					subTitle: err.message,

					buttons: ['OK']

				});

				alert.present();
			});

		});
	}
}
