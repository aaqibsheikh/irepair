import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegularOrderPage } from './regular-order';

@NgModule({
  declarations: [
    RegularOrderPage,
  ],
  imports: [
    IonicPageModule.forChild(RegularOrderPage),
  ],
})
export class RegularOrderPageModule {}
