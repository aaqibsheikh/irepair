import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { RotaPage } from '../rota/rota' 
import 'rxjs/add/operator/map';
import { AuthService } from '../../providers/auth-service/auth-service';

@IonicPage()
@Component({
  selector: 'page-regular-order',
  templateUrl: 'regular-order.html',
})
export class RegularOrderPage {

	product:any;
	productNames: any;
	productOptions:any;
	productDescription:string;
	responseData : any;
	product_detail = {product_id_fk: "", sku: "", detail:[],user_id_fk:'',shop_id_fk:'', status:null};
	shop_id:any;
	constructor(public authService:AuthService, public loading: LoadingController, public alertCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams) {
		this.shop_id = JSON.parse(localStorage.getItem('shop_id'));
	}

	ionViewDidLoad() {
		this.getProductNames();
	}

	addProduct(){

		//Getting User ID from local storage
		let user_info = JSON.parse(localStorage.getItem('userData'));
		this.product_detail.user_id_fk = user_info.user.id;
		this.product_detail.shop_id_fk = this.shop_id;

		let loader = this.loading.create({

			content: 'Processing please wait…',

		});

		loader.present().then(() => {
			this.authService.addRegularOrder(this.product_detail).then((result) => {
				this.responseData = result;
				
				if(this.responseData.status == 'success'){
					loader.dismiss();
					let alert = this.alertCtrl.create({

						title: 'Order Place Successfully',
						buttons: [{
							text:'OK',
							handler: () => {
					          this.navCtrl.push(RotaPage);
					        }
						}]
					});

					alert.present();

				}
				else{
					let alert = this.alertCtrl.create({

						title: 'ERROR',

						subTitle: this.responseData.error,

						buttons: ['OK']

					});

					alert.present();
					loader.dismiss()
				}

			}, (err) => {
				// Error log
				loader.dismiss()
				let alert = this.alertCtrl.create({

					title: 'ERROR',

					subTitle: err.message,

					buttons: ['OK']

				});

				alert.present();
			});

		});
	}

	onSelectProduct(){

		this.shop_id = JSON.parse(localStorage.getItem('shop_id'));
		let loader = this.loading.create({
			content: 'Loading please wait…',
		});

		loader.present().then(() => {

			let product_id 	= this.product_detail.product_id_fk;

			this.authService.getProductOptions(this.shop_id,product_id).then((result) => {
				this.responseData = result;
				if(this.responseData.status == 'success'){

					this.productOptions = this.responseData.data;
					loader.dismiss();
				}
				else{
					let alert = this.alertCtrl.create({

						title: 'ERROR',

						subTitle: this.responseData.error,

						buttons: ['OK']

					});

					alert.present();
					loader.dismiss();
				}
			});
		});
    }

	getProductNames(){
		let loader = this.loading.create({

			content: 'Loading please wait…',

		});

		loader.present().then(() => {

			// Getting User ID from local storage
			let user_info = JSON.parse(localStorage.getItem('userData'));
			let user_id = user_info.user.id;
			this.shop_id = JSON.parse(localStorage.getItem('shop_id'));
			this.authService.getProductNames(user_id,this.shop_id).then((result) => {
				this.responseData = result;
				if(this.responseData.status == 'success'){

					this.productNames = this.responseData.data;
					loader.dismiss();
				}
				else{
					let alert = this.alertCtrl.create({

						title: 'ERROR',

						subTitle: this.responseData.error,

						buttons: ['OK']

					});

					alert.present();
					loader.dismiss();
				}
			});
		});
	}
}
