var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
/**
 * Generated class for the RegularOrderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var RegularOrderPage = /** @class */ (function () {
    function RegularOrderPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.shopNames = ['shop1', 'shop2', 'shop3', 'shop4'];
        this.productNames = ['product1', 'product2', 'product3', 'product4'];
        this.productOptions = ['proudctoption1', 'proudctoption2', 'proudctoption3', 'proudctoption4'];
    }
    RegularOrderPage.prototype.ionViewDidLoad = function () {
    };
    RegularOrderPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-regular-order',
            templateUrl: 'regular-order.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams])
    ], RegularOrderPage);
    return RegularOrderPage;
}());
export { RegularOrderPage };
//# sourceMappingURL=regular-order.js.map