import { Component, ViewChild } from '@angular/core';
import { Nav, IonicPage, NavController, NavParams, App} from 'ionic-angular';
import { AuthService } from '../../providers/auth-service/auth-service';

import { RotaPage } from '../../pages/rota/rota';
import { RegularOrderPage } from '../../pages/regular-order/regular-order';
import { EmergencyOrderListPage } from '../../pages/emergency-order-list/emergency-order-list';
import { ComplainListPage } from '../../pages/complain-list/complain-list';
import { IssueListPage } from '../../pages/issue-list/issue-list';
import { LoginPage } from '../../pages/login/login';


@IonicPage()
@Component({
	selector: 'page-menu',
	templateUrl: 'menu.html',
})
export class MenuPage {

	@ViewChild(Nav) nav: Nav;

	activePage:any;
	shop_id:any;

	pages: Array<{title: string, page: any, icon:any}>;

	constructor(public navCtrl: NavController, public navParams: NavParams, private authService:AuthService, public appCtrl:App) {
		this.shop_id = localStorage.getItem('shop_id');	
	}

	ionViewWillEnter() {
		if(this.authService.isAdmin()){
			this.pages = [
			{ title: 'Complain', page: ComplainListPage, icon: 'md-bulb' },
			];
			this.openPage(ComplainListPage);
		}
		else if(this.authService.isBDM()){
			this.pages = [
			{ title: 'Complain', page: ComplainListPage, icon: 'md-bulb' },
			];
			this.openPage(ComplainListPage);
		}
		else if(this.authService.isManager()){
			this.pages = [
			{ title: 'Rota', page: RotaPage, icon: 'md-bulb' },
			{ title: 'Complain', page: ComplainListPage, icon: 'md-bulb' },
			{ title: 'Emergency Order', page: EmergencyOrderListPage, icon: 'md-list-box' },
			];
			this.openPage(RotaPage);
		}
		else if(this.authService.isSalesman()){
			if(this.shop_id == 0){
				this.pages = [
					{ title: 'Rota', page: RotaPage, icon: 'md-bulb' },
					{ title: 'Complain', page: ComplainListPage, icon: 'md-bulb' },
					{ title: 'Issue', page: IssueListPage, icon: 'md-build' },
				];	
			}else{
				this.pages = [
					{ title: 'Rota', page: RotaPage, icon: 'md-bulb' },
					{ title: 'Complain', page: ComplainListPage, icon: 'md-bulb' },
					{ title: 'Regular Order', page: RegularOrderPage, icon: 'md-list-box' },
					{ title: 'Issue', page: IssueListPage, icon: 'md-build' },
				];
			}
			this.openPage(RotaPage);
		}
		else if(this.authService.isTechnician()){
			this.pages = [
			{ title: 'Rota', page: RotaPage, icon: 'md-bulb' },
			{ title: 'Complain', page: ComplainListPage, icon: 'md-bulb' },
			{ title: 'Regular Order', page: RegularOrderPage, icon: 'md-list-box' },
			];
			this.openPage(RotaPage);
		}

		this.activePage = this.pages[0];
	}

	openPage(page) {
		this.nav.setRoot(page);
		this.activePage = page;
	}

	checkActive(page){
		return page == this.activePage;

	}

	logout(){
		localStorage.clear();
		this.navCtrl.push(LoginPage);
	}
}
