import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import 'rxjs/add/operator/map';

import { AuthService } from '../../providers/auth-service/auth-service';
import { ComplainListPage } from '../complain-list/complain-list';

 @IonicPage()
 @Component({
	selector: 'page-complain',
	templateUrl: 'complain.html',
 })
 export class ComplainPage {

 	responseData : any;
	complain = {title: "",description: "",user_id:''};

	constructor(public authService:AuthService, public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public loading: LoadingController) {
	}

	addComplain() {
		// Getting User ID from local storage
		let user_info = JSON.parse(localStorage.getItem('userData'));
		this.complain.user_id = user_info.user.id;

		if (this.complain.title == '') {

			let alert = this.alertCtrl.create({

				title: 'ATTENTION',

				subTitle: 'Title field is empty',

				buttons: ['OK']

			});

			alert.present();

		} else if (this.complain.description == '') {

			let alert = this.alertCtrl.create({

				title: 'ATTENTION',

				subTitle: 'Description field is empty',

				buttons: ['OK']

			});

			alert.present();

		} else{

			let loader = this.loading.create({

				content: 'Processing please wait…',

			});

			loader.present().then(() => {

				this.authService.addComplain(this.complain).then((result) => {
					this.responseData = result;
					
					if(this.responseData.status == 'success'){
						loader.dismiss();
						this.navCtrl.push(ComplainListPage);

					}
					else{
						let alert = this.alertCtrl.create({

							title: 'ERROR',

							subTitle: this.responseData.error,

							buttons: ['OK']

						});

						alert.present();
						loader.dismiss()
					}

				}, (err) => {
					// Error log
					loader.dismiss()
					let alert = this.alertCtrl.create({

						title: 'ERROR',

						subTitle: err.message,

						buttons: ['OK']

					});

					alert.present();
				});

			});
		}

	}

}
