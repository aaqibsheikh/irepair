import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import 'rxjs/add/operator/map';
import { AuthService } from '../../providers/auth-service/auth-service';
import { MenuPage } from '../menu/menu';

@IonicPage()
@Component({
	selector: 'page-login',
	templateUrl: 'login.html',
})
export class LoginPage {
	
	responseData : any;
	userData = {email: "",password: ""};

	data:string

	constructor(private authService:AuthService, public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public loading: LoadingController) {

		// If User is already Login then get its detail from local storage i.e email and Password
		if(localStorage.getItem('userData')){
			this.navCtrl.setRoot(MenuPage);
		}
	}

	signin() {
		if (this.userData.email == '') {

			let alert = this.alertCtrl.create({

				title: 'ATTENTION',

				subTitle: 'email field is empty',

				buttons: ['OK']

			});

			alert.present();

		} else if (this.userData.password == '') {

			let alert = this.alertCtrl.create({

				title: 'ATTENTION',

				subTitle: 'Password field is empty',

				buttons: ['OK']

			});

			alert.present();

		} else{

			let loader = this.loading.create({

				content: 'Processing please wait…',

			});

			loader.present().then(() => {

				this.authService.login(this.userData).then((result) => {
					this.responseData = result;
					let user_id = this.responseData.data['user']; // User ID

					if(this.responseData.status == 'success'){
						loader.dismiss()
						localStorage.setItem('userData', JSON.stringify(this.responseData.data));
						localStorage.setItem('role_id', JSON.stringify(this.responseData.role_id));
						this.navCtrl.setRoot(MenuPage, {data:user_id});

					}
					else{
						let alert = this.alertCtrl.create({

							title: 'Login Failed',

							message:'Please Check your credentials',

							buttons: ['OK']

						});

						alert.present();
						loader.dismiss()
					}

				}, (err) => {
				
				});

			});
		}

	}
}
