var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Http } from '@angular/http';
import { LoadingController } from 'ionic-angular';
import 'rxjs/add/operator/map';
import { RotaPage } from '../rota/rota';
var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, navParams, alertCtrl, http, loading) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.http = http;
        this.loading = loading;
        // If User is already Login then get its detail from local storage i.e Email and Password
        if (localStorage.getItem('userData')) {
            this.navCtrl.setRoot(RotaPage);
        }
    }
    LoginPage.prototype.ionViewDidLoad = function () {
    };
    LoginPage.prototype.signin = function () {
        if (this.email.value == '') {
            var alert_1 = this.alertCtrl.create({
                title: 'ATTENTION',
                subTitle: 'Email field is empty',
                buttons: ['OK']
            });
            alert_1.present();
        }
        else if (this.password.value == '') {
            var alert_2 = this.alertCtrl.create({
                title: 'ATTENTION',
                subTitle: 'Password field is empty',
                buttons: ['OK']
            });
            alert_2.present();
        }
        else {
            this.navCtrl.setRoot(RotaPage);
            // var headers = new Headers();
            // headers.append('Accept', 'application / json');
            // headers.append('Content - Type', 'application / json');
            // let options = new RequestOptions({
            //  headers: headers
            // });
            var data = {
                email: this.email.value,
                password: this.password.value
            };
            localStorage.setItem('userData', JSON.stringify(data));
            // let loader = this.loading.create({
            //  content: 'Processing please wait…',
            // });
            // loader.present().then(() => {
            //   this.http.post('http: //ionicdon.com/mobile/login.php',data,options)
            //    .map(res => res.json())
            //    .subscribe(res => {
            //     console.log(res)
            //     loader.dismiss()
            //     if (res == 'Your Login success') {
            //      let alert = this.alertCtrl.create({
            //       title: 'CONGRATS',
            //       subTitle: (res),
            //       buttons: ['OK']
            //      });
            //      alert.present();
            //     } else
            //     {
            //      let alert = this.alertCtrl.create({
            //       title: 'ERROR',
            //       subTitle: 'Your Login Email or Password is invalid',
            //       buttons: ['OK']
            //      });
            //      alert.present();
            //     }
            //    });
            //   });
        }
    };
    __decorate([
        ViewChild('email'),
        __metadata("design:type", Object)
    ], LoginPage.prototype, "email", void 0);
    __decorate([
        ViewChild('password'),
        __metadata("design:type", Object)
    ], LoginPage.prototype, "password", void 0);
    LoginPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-login',
            templateUrl: 'login.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, AlertController, Http, LoadingController])
    ], LoginPage);
    return LoginPage;
}());
export { LoginPage };
//# sourceMappingURL=login.js.map