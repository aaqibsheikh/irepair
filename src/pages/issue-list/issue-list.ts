import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import 'rxjs/add/operator/map';

import { AuthService } from '../../providers/auth-service/auth-service';
import { IssuePage } from '../issue/issue';
import { IssueDiscussionPage } from '../issue-discussion/issue-discussion';
import * as firebase from 'Firebase';

@IonicPage()
@Component({
  selector: 'page-issue-list',
  templateUrl: 'issue-list.html',
})
export class IssueListPage {

	responseData : any;
 	issue_list:any;
 	no_issue:string;
 	ref = firebase.database().ref('irepair/');


	constructor(public authService:AuthService, public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public loading: LoadingController){
		this.getIssue();
	}

	openIssue(issue){
		let newData = this.ref.push();
		newData.set({
			issue_name:issue
		});
		this.navCtrl.push(IssueDiscussionPage, {issue:issue});
	}

	addIssue(){
		this.navCtrl.push(IssuePage);
	}

	getIssue(){

		// Getting User ID from local storage
		let user_info = JSON.parse(localStorage.getItem('userData'));
		let role_id = JSON.parse(localStorage.getItem('role_id'));
		let user_id = user_info.user.id;


		let loader = this.loading.create({
			content: 'Processing please wait…'
		});

		loader.present().then(() => {

			this.authService.getIssue(user_id).then((result) => {
				this.responseData = result;
		
				if(this.responseData.status == 'success'){
					if(this.responseData.data == ''){
						this.no_issue = 'No Issue Created Yet';
						loader.dismiss();
					}else{

						if(this.responseData.role_id == role_id)
							this.issue_list = this.responseData.data;
						else
							this.no_issue = 'No Issue Created Yet';

						loader.dismiss();
					}

				}
				else{
					let alert = this.alertCtrl.create({

						title: 'ERROR',

						subTitle: this.responseData.error,

						buttons: ['OK']

					});

					alert.present();
					loader.dismiss()
				}

			}, (err) => {
				// Error log
				loader.dismiss()
				let alert = this.alertCtrl.create({

					title: 'ERROR',

					subTitle: err.message,

					buttons: ['OK']

				});

				alert.present();
			});

		});
	}

}
