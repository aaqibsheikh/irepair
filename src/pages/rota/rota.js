var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
var RotaPage = /** @class */ (function () {
    function RotaPage(navCtrl) {
        this.navCtrl = navCtrl;
        // used for an example of ngFor and navigation
        this.rota = [
            { title: 'Monday', desc: "hello this is day wise rota" },
            { title: 'Tuesday', desc: "hello this is day wise rota" },
            { title: 'Wednesday', desc: "hello this is day wise rota" },
            { title: 'Thursday', desc: "hello this is day wise rota" },
            { title: 'Friday', desc: "hello this is day wise rota" },
            { title: 'Saturday', desc: "hello this is day wise rota" },
            { title: 'Sunday', desc: "hello this is day wise rota" }
        ];
    }
    RotaPage = __decorate([
        Component({
            selector: 'page-rota',
            templateUrl: 'rota.html'
        }),
        __metadata("design:paramtypes", [NavController])
    ], RotaPage);
    return RotaPage;
}());
export { RotaPage };
//# sourceMappingURL=rota.js.map