import { Component } from '@angular/core';
import { NavController, NavParams,AlertController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import 'rxjs/add/operator/map';
import { AuthService } from '../../providers/auth-service/auth-service';
@Component({
	selector: 'page-rota',
	templateUrl: 'rota.html'
})
export class RotaPage {
	rota: Array<{name: string, description: any}>;
	no_rota:string;
	responseData : any;

	constructor(public authService:AuthService, public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public loading: LoadingController) {
	}

	ionViewDidLoad() {
		this.getRotaDetail();
	}

	getRotaDetail(){
		
		// Getting User ID from local storage
		let user_info = JSON.parse(localStorage.getItem('userData'));
		let user_id = user_info.user.id;

		let loader = this.loading.create({

			content: 'Loading please wait…',

		});

		loader.present().then(() => {

			this.authService.getRotaDetailByUserID(user_id).then((result) => {
				this.responseData = result;
				if(this.responseData.status == 'success'){

					localStorage.setItem('shop_id', this.responseData.shop_id);
					if(this.responseData.data == 'No Rota Set for this week'){
						this.no_rota = 'No Rota Set for this week';
					}
					else{
						this.rota = this.responseData.data;
						
						var weekday = new Array("Sunday","Monday","Tuesday","Wednesday",
							"Thursday","Friday","Saturday")

						this.rota.forEach((item, index) => {
							var d = new Date(item['date']);
							var n = weekday[d.getDay()];
							this.rota[index]['dayName'] = n;
						});
					}
					

					loader.dismiss();
				}
				else{
					let alert = this.alertCtrl.create({

						title: 'ERROR',

						subTitle: this.responseData.error,

						buttons: ['OK']

					});

					alert.present();
					loader.dismiss();
				}
			});
		});
	}
}
