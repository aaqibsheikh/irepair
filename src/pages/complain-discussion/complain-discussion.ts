import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Content } from 'ionic-angular';
import { ComplainListPage } from '../complain-list/complain-list';
import * as firebase from 'Firebase';


@IonicPage()
@Component({
  selector: 'page-complain-discussion',
  templateUrl: 'complain-discussion.html',
})
export class ComplainDiscussionPage {

	@ViewChild(Content) content: any;

	data = { type:'', nickname:'', message:'' };
	chats = [];
	roomkey:string;
	nickname:string;
	offStatus:boolean = false;
	u:any = JSON.parse(localStorage.getItem('userData'));




	constructor(public navCtrl: NavController, public navParams: NavParams) {
		this.roomkey = this.navParams.get("complain") as string;
		this.data.type = 'message';
		this.data.nickname = this.u.user.name;

		let joinData = firebase.database().ref('complains/'+this.roomkey+'/chats').push();
		joinData.set({
			type:'join',
			user:this.u.user.name,
			message:this.u.user.name+' has joined this room.',
			sendDate:Date()
		});
		this.data.message = '';

		firebase.database().ref('complains/'+this.roomkey+'/chats').on('value', resp => {
			this.chats = [];
			this.chats = snapshotToArray(resp);
			setTimeout(() => {
				if(this.offStatus === false) {
					this.content.scrollToBottom(300);
				}
			}, 1000);
		});
	}

	sendMessage() {
		let newData = firebase.database().ref('complains/'+this.roomkey+'/chats').push();
		newData.set({
			type:this.data.type,
			user:this.data.nickname,
			message:this.data.message,
			sendDate:Date()
		});
		this.data.message = '';
	}

	exitChat() {
		let exitData = firebase.database().ref('complains/'+this.roomkey+'/chats').push();
		exitData.set({
			type:'exit',
			user:this.u.user.name,
			message:this.u.user.name+' has exited this room.',
			sendDate:Date()
		});

		this.offStatus = true;

		this.navCtrl.setRoot(ComplainListPage, {
			nickname:this.u.user.name
		});
	}

}
export const snapshotToArray = snapshot => {
	let returnArr = [];

	snapshot.forEach(childSnapshot => {
		let item = childSnapshot.val();
		item.key = childSnapshot.key;
		returnArr.push(item);
	});

	return returnArr;
};
