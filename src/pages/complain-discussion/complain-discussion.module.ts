import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ComplainDiscussionPage } from './complain-discussion';

@NgModule({
  declarations: [
    ComplainDiscussionPage,
  ],
  imports: [
    IonicPageModule.forChild(ComplainDiscussionPage),
  ],
})
export class ComplainDiscussionPageModule {}
