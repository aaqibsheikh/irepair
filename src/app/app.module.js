var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MyApp } from './app.component';
import { RotaPage } from '../pages/rota/rota';
import { LoginPage } from '../pages/login/login';
import { RegularOrderPage } from '../pages/regular-order/regular-order';
import { ComplainPage } from '../pages/complain/complain';
import { IssuePage } from '../pages/issue/issue';
import { IssueListPage } from '../pages/issue-list/issue-list';
import { IssueDiscussionPage } from '../pages/issue-discussion/issue-discussion';
import { AccordionComponent } from '../components/accordion/accordion';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        NgModule({
            declarations: [
                MyApp,
                RotaPage,
                LoginPage,
                RegularOrderPage,
                ComplainPage,
                IssuePage,
                IssueListPage,
                IssueDiscussionPage,
                AccordionComponent
            ],
            imports: [
                BrowserModule,
                IonicModule.forRoot(MyApp),
                BrowserAnimationsModule,
                HttpModule,
                MatSelectModule,
                MatInputModule,
                MatButtonModule
            ],
            bootstrap: [IonicApp],
            entryComponents: [
                MyApp,
                RotaPage,
                LoginPage,
                RegularOrderPage,
                ComplainPage,
                IssuePage,
                IssueListPage,
                IssueDiscussionPage
            ],
            providers: [
                StatusBar,
                SplashScreen,
                { provide: ErrorHandler, useClass: IonicErrorHandler }
            ]
        })
    ], AppModule);
    return AppModule;
}());
export { AppModule };
//# sourceMappingURL=app.module.js.map