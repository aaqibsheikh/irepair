import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule } from '@angular/http';


import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';

import { MyApp } from './app.component';
import { RotaPage } from '../pages/rota/rota';
import { LoginPage } from '../pages/login/login';
import { RegularOrderPage } from '../pages/regular-order/regular-order';
import { EmergencyOrderPage } from '../pages/emergency-order/emergency-order';
import { EmergencyOrderListPage } from '../pages/emergency-order-list/emergency-order-list';
import { ComplainPage } from '../pages/complain/complain';
import { ComplainListPage } from '../pages/complain-list/complain-list';
import { IssuePage } from '../pages/issue/issue';
import { MenuPage } from '../pages/menu/menu';
import { IssueListPage } from '../pages/issue-list/issue-list';
import { IssueDiscussionPage } from '../pages/issue-discussion/issue-discussion';
import { ComplainDiscussionPage } from '../pages/complain-discussion/complain-discussion';
import { AccordionComponent } from '../components/accordion/accordion';


import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AuthService } from '../providers/auth-service/auth-service';

@NgModule({
  declarations: [
    MyApp,
    RotaPage,
    LoginPage,
    RegularOrderPage,
    EmergencyOrderPage,
    ComplainPage,
    ComplainListPage,
    IssuePage,
    IssueListPage,
    IssueDiscussionPage,
    ComplainDiscussionPage,
    EmergencyOrderListPage,
    AccordionComponent,
    MenuPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    BrowserAnimationsModule,
    HttpModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatButtonModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    RotaPage,
    LoginPage,
    RegularOrderPage,
    EmergencyOrderPage,
    ComplainPage,
    ComplainListPage,
    IssuePage,
    IssueListPage,
    IssueDiscussionPage,
    ComplainDiscussionPage,
    EmergencyOrderListPage,
    MenuPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthService
  ]
})
export class AppModule {}
