import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import * as firebase from 'firebase';

import { LoginPage } from '../pages/login/login';

  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyB2m0xJ9pjwhtYod5I8C1b3KwEIkKQSVWA",
    authDomain: "repair-daf45.firebaseapp.com",
    databaseURL: "https://repair-daf45.firebaseio.com",
    projectId: "repair-daf45",
    storageBucket: "",
    messagingSenderId: "766811964159"
  };

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = LoginPage;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      firebase.initializeApp(config);
    });
  }
}
