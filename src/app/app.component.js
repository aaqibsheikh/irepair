var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import * as firebase from 'firebase';
import { RotaPage } from '../pages/rota/rota';
import { LoginPage } from '../pages/login/login';
import { RegularOrderPage } from '../pages/regular-order/regular-order';
import { ComplainPage } from '../pages/complain/complain';
import { IssuePage } from '../pages/issue/issue';
import { IssueListPage } from '../pages/issue-list/issue-list';
// Initialize Firebase
var config = {
    apiKey: "AIzaSyCs7QBFe2Rtw8lj3Lzrhlcv1Z_D5Z7rxA8",
    authDomain: "irepair-49f48.firebaseapp.com",
    databaseURL: "https://irepair-49f48.firebaseio.com",
    projectId: "irepair-49f48",
    storageBucket: "irepair-49f48.appspot.com",
    messagingSenderId: "658201185684"
};
var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.rootPage = LoginPage;
        this.initializeApp();
        // used for an example of ngFor and navigation
        this.pages = [
            { title: 'Rota', component: RotaPage, icon: 'md-browsers' },
            { title: 'Regular Order', component: RegularOrderPage, icon: 'md-list-box' },
            { title: 'Complain', component: ComplainPage, icon: 'md-bulb' },
            { title: 'Issue', component: IssuePage, icon: 'md-build' },
            { title: 'Issue List', component: IssueListPage, icon: 'ios-create' },
        ];
        this.activePage = this.pages[0];
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
            firebase.initializeApp(config);
        });
    };
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
        this.activePage = page;
    };
    MyApp.prototype.checkActive = function (page) {
        return page == this.activePage;
    };
    __decorate([
        ViewChild(Nav),
        __metadata("design:type", Nav)
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Component({
            templateUrl: 'app.html'
        }),
        __metadata("design:paramtypes", [Platform, StatusBar, SplashScreen])
    ], MyApp);
    return MyApp;
}());
export { MyApp };
//# sourceMappingURL=app.component.js.map