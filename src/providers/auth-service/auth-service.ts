import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';


let apiUrl = 'http://shalimarphuket.com/irepair/api/';

@Injectable()
export class AuthService{
	role_id:any;
	constructor(public http: Http) {
	}

	// isLoggedIn(){
	// 	console.log('chk_null'+this.current_user_check);
	// 	return this.current_user_check != undefined;
	// }

	isAdmin(){
		if(localStorage.getItem('role_id')){
			this.role_id = JSON.parse(localStorage.getItem('role_id'));
		}
		return this.role_id == 1;
	}

	isBDM(){
		return this.role_id == 2;
	}
	
	isManager(){
		return this.role_id == 3;
	}


	isSalesman(){
		return this.role_id == 4;
	}

	isTechnician(){
		return this.role_id == 5;
	}

	login(credentials) {
		return new Promise((resolve, reject) => {
			let headers = new Headers({
				'content-type': 'application/json',
				'Accept': 'application/json',
				'Access-Control-Allow-Credentials':true,
				'Access-Control-Allow-Origin':true
			});

			this.http.post(apiUrl+'login', JSON.stringify(credentials), {headers: headers})
			.subscribe(res => {
				resolve(res.json());
			}, (err) => {
				reject(err);
			});
		});
	}


	// Getting Rota by user id
	getRotaDetailByUserID(user_id) {
		return new Promise((resolve, reject) => {
			let headers = new Headers({
				'content-type': 'application/json',
				'Accept': 'application/json',
				'Access-Control-Allow-Credentials':true,
				'Access-Control-Allow-Origin':true
			});

			this.http.post(apiUrl+'getRotaDetail', {user_id:user_id}, {headers: headers})
			.subscribe(res => {
				resolve(res.json());
			}, (err) => {
				reject(err);
			});
		});
	}

	getProductNames(user_id,shop_id) {
		return new Promise((resolve, reject) => {
			let headers = new Headers({
				'content-type': 'application/json',
				'Accept': 'application/json',
				'Access-Control-Allow-Credentials':true,
				'Access-Control-Allow-Origin':true
			});

			this.http.post(apiUrl+'getProductNames', {user_id:user_id,shop_id:shop_id}, {headers: headers})
			.subscribe(res => {
				resolve(res.json());
			}, (err) => {
				reject(err);
			});
		});
	}

	getProductOptions(shop_id,product_id){
		return new Promise((resolve, reject) => {
			let headers = new Headers({
				'content-type': 'application/json',
				'Accept': 'application/json',
				'Access-Control-Allow-Credentials':true,
				'Access-Control-Allow-Origin':true
			});

			this.http.post(apiUrl+'getProductOptions', {shop_id:shop_id, product_id:product_id}, {headers: headers})
			.subscribe(res => {
				resolve(res.json());
			}, (err) => {
				reject(err);
			});
		});
	}

	addComplain(complain) {
		return new Promise((resolve, reject) => {
			let headers = new Headers({
				'content-type': 'application/json',
				'Accept': 'application/json',
				'Access-Control-Allow-Credentials':true,
				'Access-Control-Allow-Origin':true
			});

			this.http.post(apiUrl+'addComplain', JSON.stringify(complain), {headers: headers})
			.subscribe(res => {
				resolve(res.json());
			}, (err) => {
				reject(err);
			});
		});
	}

	getComplain() {
		return new Promise((resolve, reject) => {
			let headers = new Headers({
				'content-type': 'application/json',
				'Accept': 'application/json',
				'Access-Control-Allow-Credentials':true,
				'Access-Control-Allow-Origin':true
			});

			this.http.post(apiUrl+'getComplain', '', {headers: headers})
			.subscribe(res => {
				resolve(res.json());
			}, (err) => {
				reject(err);
			});
		});
	}

	addIssue(issue) {
		return new Promise((resolve, reject) => {
			let headers = new Headers({
				'content-type': 'application/json',
				'Accept': 'application/json',
				'Access-Control-Allow-Credentials':true,
				'Access-Control-Allow-Origin':true
			});

			this.http.post(apiUrl+'addIssue', JSON.stringify(issue), {headers: headers})
			.subscribe(res => {
				resolve(res.json());
			}, (err) => {
				reject(err);
			});
		});
	}

	getIssue(user_id) {
		return new Promise((resolve, reject) => {
			let headers = new Headers({
				'content-type': 'application/json',
				'Accept': 'application/json',
				'Access-Control-Allow-Credentials':true,
				'Access-Control-Allow-Origin':true
			});

			this.http.post(apiUrl+'getIssue', {user_id:user_id}, {headers: headers})
			.subscribe(res => {
				resolve(res.json());
			}, (err) => {
				reject(err);
			});
		});
	}

	addRegularOrder(product_detail) {
		return new Promise((resolve, reject) => {
			let headers = new Headers({
				'content-type': 'application/json',
				'Accept': 'application/json',
				'Access-Control-Allow-Credentials':true,
				'Access-Control-Allow-Origin':true
			});

			this.http.post(apiUrl+'place_regular_order', JSON.stringify(product_detail), {headers: headers})
			.subscribe(res => {
				resolve(res.json());
			}, (err) => {
				reject(err);
			});
		});
	}

	placeEmergencyOrder(product_detail) {
		return new Promise((resolve, reject) => {
			let headers = new Headers({
				'content-type': 'application/json',
				'Accept': 'application/json',
				'Access-Control-Allow-Credentials':true,
				'Access-Control-Allow-Origin':true
			});

			this.http.post(apiUrl+'place_emergency_order', JSON.stringify(product_detail), {headers: headers})
			.subscribe(res => {
				resolve(res.json());
			}, (err) => {
				reject(err);
			});
		});
	}

	getEmergencyOrderList() {
		return new Promise((resolve, reject) => {
			let headers = new Headers({
				'content-type': 'application/json',
				'Accept': 'application/json',
				'Access-Control-Allow-Credentials':true,
				'Access-Control-Allow-Origin':true
			});

			this.http.post(apiUrl+'getEmergencyOrderList', '', {headers: headers})
			.subscribe(res => {
				resolve(res.json());
			}, (err) => {
				reject(err);
			});
		});
	}


}